exports.definition = {
    config: {
        columns: {
            owned_id: "INTEGER PRIMARY KEY AUTOINCREMENT",
            name: "TEXT"
        },
        adapter: {
            type: "sql",
            collection_name: "owned",
            idAttribute: "owned_id",
            db_name: "retrogames"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("owned", exports.definition, [ function(migration) {
    migration.name = "owned";
    migration.id = "20140606163615";
    var preload_data = [ {
        name: ""
    }, {
        name: "Wanted!"
    }, {
        name: "Owned"
    } ];
    migration.up = function(migrator) {
        migrator.createTable({
            columns: {
                platform_id: "INTEGER PRIMARY KEY AUTOINCREMENT",
                name: "TEXT"
            }
        });
        for (var i = 0; preload_data.length > i; i++) migrator.insertRow(preload_data[i]);
    };
    migration.down = function(migrator) {
        migrator.dropTable();
    };
} ]);

collection = Alloy.C("owned", exports.definition, model);

exports.Model = model;

exports.Collection = collection;