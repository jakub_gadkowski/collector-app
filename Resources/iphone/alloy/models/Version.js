exports.definition = {
    config: {
        columns: {
            version_id: "INTEGER PRIMARY KEY AUTOINCREMENT",
            name: "TEXT"
        },
        adapter: {
            type: "sql",
            collection_name: "versions",
            idAttribute: "version_id",
            db_name: "retrogames"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("version", exports.definition, [ function(migration) {
    migration.name = "version";
    migration.id = "20140606163615";
    var preload_data = [ {
        name: "NES"
    }, {
        name: "PS"
    }, {
        name: "PC"
    } ];
    migration.up = function(migrator) {
        migrator.createTable({
            columns: {
                platform_id: "INTEGER PRIMARY KEY AUTOINCREMENT",
                name: "TEXT"
            }
        });
        for (var i = 0; preload_data.length > i; i++) migrator.insertRow(preload_data[i]);
    };
    migration.down = function(migrator) {
        migrator.dropTable();
    };
} ]);

collection = Alloy.C("version", exports.definition, model);

exports.Model = model;

exports.Collection = collection;