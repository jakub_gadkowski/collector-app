var preload_data = [
            {
                "name" : "Mario Bros",
                "publisher" : "Nintendo",
                "developer" : "Nintendo",
                "platform" : "1",
                "rarity" : "4",
                "type" : "1",
                "wanted" : "0",
                "year" : "1987"
            },
            {
                "name" : "Oblivion",
                "publisher" : "Bethesda",
                "developer" : "Some guy",
                "platform" : "2",
                "rarity" : "2",
                "type" : "1",
                "wanted" : "0",
                "year" : "2006"
            },
            {
                "name" : "Dragon Age",
                "publisher" : "Electronic Arts",
                "developer" : "Not me",
                "platform" : "2",
                "rarity" : "1",
                "type" : "1",
                "wanted" : "0",
                "year" : "2009"
            }
];

migration.up = function(migrator)
{
    migrator.createTable({
        "columns":
        {
            "game_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
            "name" : "TEXT",
            "publisher" : "TEXT",
            "developer" : "TEXT",
            "year" : "INTEGER",
            "platform" : "INTEGER",
            "rarity" : "INTEGER",
            "min_price" : "INTEGER",
            "av_price" : "INTEGER",
            "max_price" : "INTEGER",
            "notes" : "TEXT",
            "wanted" : "INTEGER",
            "version" : "INTEGER",
            "type" : "INTEGER"
        }
    });
    for (var a=0;a<=100;a++){
        for (var i = 0; i < preload_data.length; i++) {
            migrator.insertRow(preload_data[i]);
        }
    }
};

migration.down = function(migrator)
{
    migrator.dropTable();
};