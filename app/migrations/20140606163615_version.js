var preload_data = [
            {
                "name" : "NES",
            },
            {
                "name" : "PS",
            },
            {
                "name" : "PC",
            }
];

migration.up = function(migrator)
{
    migrator.createTable({
        "columns":
        {
            "platform_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
            "name" : "TEXT"
        }
    });
    for (var i = 0; i < preload_data.length; i++) {
        migrator.insertRow(preload_data[i]);
    }
};

migration.down = function(migrator)
{
    migrator.dropTable();
};