var preload_data = [
            {
                "name" : "Common",
                "shortName" : "C"
            },
            {
                "name" : "Uncommon",
                "shortName" : "U"
            },
            {
                "name" : "Rare",
                "shortName" : "R"
            },
            {
                "name" : "Ultra Rare",
                "shortName" : "UR"
            }
];

migration.up = function(migrator)
{
    migrator.createTable({
        "columns":
        {
            "rarity_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
            "name" : "TEXT",
            "shortName" : "TEXT"
        }
    });
    for (var i = 0; i < preload_data.length; i++) {
        migrator.insertRow(preload_data[i]);
    }
};

migration.down = function(migrator)
{
    migrator.dropTable();
};