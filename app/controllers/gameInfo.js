/* exported updateFromTextField, handleTouches*/
/*************************
**         Init         **
**************************/
var args = arguments[0] || {};
//collections needed for relations for 'games' model
var rarityCollection = Alloy.createCollection('rarity');
rarityCollection.fetch();
var platformCollection = Alloy.createCollection('platforms');
platformCollection.fetch();
var interfaceTable = [
		{
			item : $.mainWindow,
			modelField : 'name'
		},
		{
			item : $.nameField,
			modelField : 'name'
		},
		{
			item : $.platformLabel,
			modelField : 'platformName'
		},
		{
			item : $.publisherLabel,
			modelField : 'publisher'
		},
		{
			item : $.developerLabel,
			modelField : 'developer'
		},
		{
			item : $.releasedLabel,
			modelField : 'year'
		},
		{
			item : $.rarityLabel,
			modelField : 'rarityName'
		},
		{
			item : $.minValField,
			modelField : 'min_price'
		},
		{
			item : $.averageValField,
			modelField : 'av_price'
		},
		{
			item : $.maxValField,
			modelField : 'max_price'
		},
		{
			item : $.notesArea,
			modelField : 'notes'
		},
		{
			item : $.wantedSwitch,
			modelField : 'wanted'
		}
	];

// var game = {
// 	'id' : args.model.id,
// 	'name' : args.model.get('name'),
//     'publisher' : args.model.get('publisher'),
//     'year' : args.model.get('year'),
//     'platform' : args.model.get('platform'),
//     'rarity' : args.model.get('rarity'),
//     'min_price' : args.model.get('min_price'),
//     'av_price' : args.model.get('av_price'),
//     'max_price' : args.model.get('max_price'),
//     'notes' : args.model.get('notes'),
//     'wanted' : args.model.get('wanted'),
//     'version' : args.model.get('version'),
//     'type' : args.model.get('type'),
//     'rarityName' : args.model.get('rarityName'),
//     'rarityShort' : args.model.get('rarityShort'),
//     'platformName' : args.model.get('platformName'),
//     'platformShort' : args.model.get('platformShort'),
//     /**
//      * Updates Game object with new value for given attribute and repopulate interface. 
//      * @method update
//      * @param  {Object} options - 'attribute' - attribute to update, 'value' - new value for that attribute. 
//      * @return {void}
//      */
//     update : function (options) {
//     	options = options || {};
//     	var attribute = options.attribute || null;
//     	var newValue = options.value || null;
//     	if (attribute) {
//     		this.attribute = newValue;
// 			populateInterface();
// 		}
    	
//     }
// };



var populateInterface = {
	interfaceTable : [],
	init : function() {
		//get list of interface items
		this.interfaceTable = interfaceTable;
		//update all interface items with model data
		this.updateAll();
	},
	updateAll : function() {
		this.interfaceTable.forEach(function(el) {
			this.populateField( el.item, this.getModelValue(el.modelField));
		}, this);
	},
	updateOne : function(item) {
		this.interfaceTable.some(function(el) {
			if (el.modelField === item)	{
				this.populateField( el.item, this.getModelValue(el.modelField));
				return el;
			}
		}, this);
	},
	getModelValue :function	(modelField) {
		// if model field is type 'wanted': return values for Ti.UI.Switch (bool), otherwise get value from model field
		return (modelField === 'wanted' ? (args.model.get('wanted') ? true : false) : args.model.get(modelField));
	},
	populateField : function (item, value) {
		switch (item.apiName) {
			case 'Ti.UI.Label' :
				item.opacity = 0.0;
				item.text = value;
				item.animate({
					opacity : 1.0,
					duration : 250
				});
				break;
			case 'Ti.UI.Window' :
			case 'Ti.UI.View' :
				item.setTitle(value);
				break;
			case 'Ti.UI.TextField' :
			case 'Ti.UI.TextArea' :
			case 'Ti.UI.Switch' :
				// If matching price fields
				if (item.id.match(/ValField/g)) {
					//Test for number and return it or return 0.00
					item.setValue(testNumber(value));
				} else {
					item.setValue(value);
				}
				
				break;
			default :
				break;
		}
	},
	release : function() {}
};


function handleTouches (e) {
	var data = {
  				cancel: null,
  				options: ['Cancel'],
  				selectedIndex: null,
				destructive: -1,
  				title: null
			};
	switch (e.source.id) {
		case 'rarityLabel' :
		//Ti.API.info(rarityCollection);
			rarityCollection.forEach(function(el) {
				data.options.unshift(el.get('name'));
			});

			data.title = 'Change Rarity';
			data.cancel = Number(data.options.length) - 1;
			data.selectedIndex = data.cancel;
			editOption(data);
			break;
		case 'wantedSwitch' : 
			updateWanted();
		break;
		case 'platformLabel' :
			showPlatformPopup();
		break;
		default :
			break;
	}
}


function showPlatformPopup () {
	var payload = {
		platformCollection : platformCollection
	};

	Alloy.createController('platformPopup', payload).show();
}

function editOption (dialogOptions) {
	//Ti.API.info(dialogOptions);
	var dialog = Ti.UI.createOptionDialog(dialogOptions);
	dialog.addEventListener('click', dialogClickHandler);
	dialog.show();
}

function dialogClickHandler (e) {
	if (e.index !== e.cancel) {
		var rarityIndex = 4 - Number(e.index); 
		var rarityModel = rarityCollection.get(rarityIndex);
		// game.update({attribute:'rarity', value: e.index});
		
		saveModel({
			'rarity': rarityIndex,
			'rarityName' : rarityModel.get('name'),
			'rarityShort' : rarityModel.get('shortName')
		});
		populateInterface.updateOne('rarityName');
	} else {
		return;
	}
}



function updateWanted () {
	var status = ($.wantedSwitch.value ? 1 : 0);
	
	saveModel({
		'wanted': status
	});
}

function updateFromTextField (e) {
	// 	Ti.API.info(e);
	var source = e.source;
	var options = {};
	var testedValue = testNumber(source.value);
	// Ti.API.info(testedValue);
	var modelField;
	var modelValue;
	switch (source.id) {
		case 'nameField' :
		case 'publisherLabel' :
		case 'developerLabel' :
		case 'releasedLabel' :
		case 'notesArea' :
			interfaceTable.forEach(function(el) {
				if(el.item === source) {
					modelField = el.modelField;
				}
			});
			modelValue = source.value;
			// If the 'Name' field was updated, also update Window title. 
			if (source.id === 'nameField') populateInterface.populateField($.mainWindow, modelValue);
			
			break;
		case 'minValField' :
			modelField = 'min_price';
			modelValue = testedValue;
			break;
		case 'averageValField' :
			modelField = 'av_price';
			modelValue = testedValue;
			break;
		case 'maxValField' :
			modelField = 'max_price';
			modelValue = testedValue;
			break;
		default :
			break;
	}
	options[modelField] = modelValue;
	saveModel(options);
	if (modelField.match(/price/g)) populateInterface.updateOne(modelField);
}


function testNumber (value) {
	value = Number(value);

	if (typeof value === 'number' && isNaN(value)) {
		return value.toFixed(2);
	} else {
		//try to make sense of it add some filtering
		return Number(0).toFixed(2);
	}
}

// function updateName (e) {
// 	Ti.API.info(e);
// 	var name = $.nameField.value;
	
// 	saveModel({
// 		'name': name
// 	})
// 	populateInterface.populateField($.mainWindow, name);
// }

// //Ti.API.info (args.model);
function saveModel (options) {
	options = options || {};
	args.model.save(options);
}

populateInterface.init();