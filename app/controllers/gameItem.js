/* exported longClickHandler */
var args = arguments[0] || {};

var opts = {
  cancel: 2,
  options: [],
  selectedIndex: 2,
  destructive: 1,
  title: 'Quick Options'
};

var clickCallback;

function longClickHandler (e) {
	e.cancelBubble = true;
	Ti.API.info(e);
	var itemModel = Alloy.Collections.games.get(e.itemId);
	generateDialog(e, itemModel);
}

function generateDialog (clickedInfo, itemModel) {
	opts.options = [ (itemModel.get('wanted') ? 'Don\'t need this.' : 'I want this!' ), 'Delete Item', 'Cancel'];
	var dialog = Ti.UI.createOptionDialog(opts);

	clickCallback = function(e) {
		dialogClickHandler(e, dialog, itemModel);
	};
	
	dialog.addEventListener('click', clickCallback);
	dialog.show();
}

function dialogClickHandler (e, dialogRef, itemModel) {
	if (e.index !== e.cancel) {
		switch (e.index) {
			case 0 :
				updateWanted(itemModel);
				break;
			case 1 :
				deletedItem(itemModel);
				break;
			default :
				break;
		}
	} 
	dialogRef.removeEventListener('click', clickCallback);
}

function updateWanted (itemModel) {
	//get wanted (bool) and toggle it.	
	var value = (itemModel.get('wanted') ? 0 : 1);

	itemModel.save({
		'wanted' : value
	});
}

function deletedItem (itemModel) {
	itemModel.destroy({wait: true});	
}