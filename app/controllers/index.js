var games = Alloy.Collections.games;
var listSection = [];
// var gameItems = [];

games.fetch({
    success: createGroupedSections,
    //TODO: add general error handling
    error: function(){
                Ti.API.error('collection fetch failed');
            }
});

games.on('change destroy', updateGameList);

$.addButton.addEventListener('click', addNewItem);

$.mainList.addEventListener('itemclick', openGameInfo);
$.filterBar.addEventListener('click', function(e) {
 	Ti.API.info(e);

 	switch (e.index) {
 		case 0 :
            createGroupedSections(games);
 		break;
 		case 1 :
 			var filtered = games.where({'wanted' : 1});
 			createGroupedSections(filtered);
 		break;
 	}
});

function addNewItem () {

	var newItem = Alloy.createModel('games');
	newItem.save();

	games.fetch({
    	success: function () {
    		var gameToEdit = {
		    	model : games.get(newItem.id),
		    	itemIndex : null,
		    	secitonIndex : null
			};

			$.index.openWindow(Alloy.createController('gameInfo', gameToEdit).getView());
    		createGroupedSections();
    	},
	});
}

function openGameInfo (e) {
	var gameToEdit = {
    	model : games.get(e.itemId),
    	itemIndex : e.itemIndex,
    	secitonIndex : e.sectionIndex
	};
    $.index.openWindow(Alloy.createController('gameInfo', gameToEdit).getView());
}


function createGroupedSections	(collection) {
		var models;

		if (collection === undefined) {
            models = games.models;
		} else if (collection.models){
		    models = collection.models;
		} else {
		    models = collection;
		}

		// Ti.API.info(collection);

		var sectionIndexItemList = [
			{
				'title': 'SearchIcon',
				'index': 0
			}
		];
		listSection = [];

		var index = 0;
		// Ti.API.info(collection);
		var groupedItems = _.groupBy(models, function (model) {
			// Ti.API.info(JSON.stringify(row));
			var name =  model.get('name');
			return name[0].toUpperCase();
		});
		//Ti.API.info(typeof groupedItems);
		for (var section in groupedItems) {

			//Ti.API.info(section);

			var createdSection = Ti.UI.createListSection({
			// properties
				items: createGameItem( groupedItems[section] ),
				headerTitle: section
				// headerView: sectionView
			});

			var sectionIndexItem = {
				'title': section,
				'index': index
			};
			++index;

			listSection.push(createdSection);
			sectionIndexItemList.push(sectionIndexItem);
		}

		$.mainList.sectionIndexTitles = sectionIndexItemList;
		$.mainList.sections = listSection;
		// Ti.API.info(grouped.length);
}



function initListView (argument) {

}

function createGameItem (itemsList) {
	var gameItems = [];

	itemsList.forEach(function (item) {
		var name = item.get('name');
		var itemProperies = {
			properties: {
				searchableText : name,
				itemId : item.id
			},
			name: {
				text: name
			},
			info: {
				text: item.get('platformName') + ' | ' + item.get('publisher') + ' | ' + item.get('year')
			},
			prices: {
				text: Number(item.get('min_price')).toFixed(2) + ' | ' + Number(item.get('av_price')).toFixed(2) + ' | ' + Number(item.get('max_price')).toFixed(2)
			},
			rarity : {
				text: '●',//item.get('rarityShort'),
				color: rarityBadgeColor(item.get('rarity'))
			},
			wanted :{
				visible: (item.get('wanted') ? true : false)
			}
		};
		gameItems.push(itemProperies);


	});
	// Ti.API.info(gameItems);
	return gameItems;
	// listSection = Ti.UI.createListSection({
 //    	// properties
	//     items: gameItems,
	//     headerTitle: "Section A"
	// });

	// $.gameList.setItems(gameItems);
	// Ti.API.info(gameItems);
	// $.mainList.sections = [listSection];
}


function rarityBadgeColor(rarity_id) {
	rarity_id = rarity_id || null;
	switch (rarity_id) {
		case 1 :
			return 'green';
		case 2 :
			return 'yellow';
		case 3 :
			return 'orange';
		case 4 :
			return 'red';
		default :
			return 'black';
	}
}

function updateGameList (updatedModel) {
	games.sort();
	createGroupedSections();
	// listSection.forEach(function(el){
	// 	var items = el.getItems();
	// 	var result = checkSectionItems(items, updatedModel);
	// 	Ti.API.info('result: ' + result);
	// 	if (result) el.updateItemAt(result.index, result.dataItem);
	// });
}

function checkSectionItems (itemList, updatedModel) {
	var result = false;
	itemList.forEach(function(el, index) {
		if (el.properties.itemId === updatedModel.id) {
			el.name.text = updatedModel.get('name');
			//Ti.API.info('returned 1');
			result = {
				'index' : index,
				'dataItem' : el
			}
		}
	});

	return result;
}

$.index.open();