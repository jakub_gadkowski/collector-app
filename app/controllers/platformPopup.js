var args = arguments[0] || {};
var self = $.platformWindow;
var platforms = args.platformCollection;

function closeWindow () {
	if(OS_IOS) {
		self.animate({
			opacity: 0.0,
			duration : 250
		}, function() {
			 self.close();
		});
	} else {
		self.close();
	}
}

function createPlatformList (itemsList) {
	var platformsItems = [];

	itemsList.forEach(function (item) {
		var name = item.get('name');
		var itemProperies = {
			properties: {
				itemId : item.id
			},
			name: {
				text: name
			}
		};
		platformsItems.push(itemProperies);
	});
	// return platformsItems;
	var listSection = Ti.UI.createListSection({
    	// properties
	    items: platformsItems
	});
	$.mainList.sections = [listSection];
}

createPlatformList(platforms);
//Exports split in IOS and Android for different opening/closing anims.
exports.show = function() {
	if(OS_IOS) {
		self.open();
		self.animate({
			opacity: 1.0,
			duration : 500
		});

	} else {
		self.open({
			activityEnterAnimation: Ti.Android.R.anim.fade_in,
            activityExitAnimation: Ti.Android.R.anim.fade_out
		});
	}
};