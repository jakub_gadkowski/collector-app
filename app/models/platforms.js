exports.definition = {
    config : {
        columns : {
            "platform_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
            "name" : "TEXT",
            "shortName" : "TEXT"
        },
        adapter : {
            "type" : "sql",
            "collection_name" : "platforms",
            "idAttribute": "platform_id",
            "db_name" : "retrogames"
        }
    },

    extendModel: function(Model) {
        _.extend(Model.prototype, {
            // Extend, override or implement Backbone.Model
        });

        return Model;
    },

    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // Extend, override or implement Backbone.Collection
        });

        return Collection;
    }

};