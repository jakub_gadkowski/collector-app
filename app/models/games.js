exports.definition = {
    config : {
        columns : {
            "game_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
            "name" : "TEXT",
            "publisher" : "TEXT",
            "developer" : "TEXT",
            "year" : "INTEGER",
            "platform" : "INTEGER",
            "rarity" : "INTEGER",
            "min_price" : "INTEGER",
            "av_price" : "INTEGER",
            "max_price" : "INTEGER",
            "notes" : "TEXT",
            "wanted" : "INTEGER",
            "version" : "INTEGER",
            "type" : "INTEGER"
        },
        defaults : {
            "name" : "New Game",
            "publisher" : "None",
            "developer" : "None",
            "year" : "1970",
            "platform" : "1",
            "rarity" : "1",
            "min_price" : "0",
            "av_price" : "0",
            "max_price" : "0",
            "notes" : "",
            "wanted" : "1",
            "version" : "1",
            "type" : "1"
        },
        adapter : {
            "type" : "sql",
            "collection_name" : "games",
            "idAttribute": "game_id",
            "db_name" : "retrogames"
        }
    },

    extendModel: function(Model) {
        var rarity = Alloy.createCollection('rarity');
        var platform = Alloy.createCollection('platforms');
        rarity.fetch();
        platform.fetch();
        
        _.extend(Model.prototype, {
            // Extend, override or implement Backbone.Model
                initialize: function(){
                    // Ti.API.info(this);
                    Model.prototype.rarity.call(this);
                    Model.prototype.platform.call(this);
                    // this.on('change',function(e){
                    //     Ti.API.info('Model Changed');
                    // });
                },

                rarity : function() {
                    // Ti.API.info(rarity);
                    var r = rarity.get(this.get('rarity'));
                    this.set({
                        'rarityName' : r.get('name'),
                        'rarityShort' : r.get('shortName')
                    }, {silent:true});
                },

                platform : function() {
                    // Ti.API.info(this);
                    var p = platform.get(this.get('platform'));
                    this.set({
                        'platformName' : p.get('name'),
                        'platformShort' : p.get('shortName')
                    }, {silent:true});    
                }
        });

        return Model;
    },

    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // Extend, override or implement Backbone.Collection
            initialize: function(){
              // this.on("all", function(eventName) {
              //     Ti.API.info(eventName);
              //   });

              this.on('change',function(e){
                    Ti.API.info('Collection Changed');
              });
                this.sortVar = 'name';
            },
            comparator : function(games) {
               var _this = this;
               return games.get( _this.sortVar ).toUpperCase();
            }
        });

        return Collection;
    }

};